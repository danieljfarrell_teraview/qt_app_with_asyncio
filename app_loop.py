import os
import sys
import asyncio
import time
import threading
import quamash
import PyQt5
# Use QT5 backend,
# https://github.com/enthought/traitsui/issues/407
os.environ['ETS_TOOLKIT'] = 'qt'
os.environ['QT_API'] = 'pyqt5' 
from traits.api import HasTraits, Any, Button, Instance, Bool, Enum, Property
from traitsui.api import ModelView, View, Item, HGroup, VGroup


def setup_event_loop():
    """ Configure asyncio with QT event loop.
        
        Note:
        This needs to be called as early as possible once
        TraitsUI has created the application instances. This
        is to avoid running asynchronous code before
        `asyncio.set_event_loop(loop)` is called.
    """
    app = PyQt5.QtWidgets.QApplication.instance()
    if app is None:
        raise RuntimeError('QApplication not yet created')
    print('QApplication: {}'.format(app))
    loop = quamash.QEventLoop(app)
    print('QEventLoop: {}'.format(loop))
    asyncio.set_event_loop(loop)
    return loop

# Constant
CONNECTION_STATES = ['Disconnected', 'Connected']

class App(HasTraits):
    """ Application that simulates monitoring connection
        status of a network resource.

    """
    
    #: Shared event loop with PyQt5
    event_loop = Instance(object)
    
    #: Ground truth of network connectivity state
    actual_connection_state = Enum(*CONNECTION_STATES)
    
    #: This trait is updated periodically by the 
    #: coroutine `check_connection_periodically`.
    connection_state = Enum(*CONNECTION_STATES)
    
    #: Causes the coroutine to exit after next cycle
    exit_check_connection_periodically = Bool(False)
    
    def __init__(self, *args, **kwargs):
        """
        """
        print('App init')
        self.event_loop = setup_event_loop()
        self.check_connection_periodically_task = self.event_loop.create_task(self.check_connection_periodically())

    async def check_connection_periodically(self):
        """ Every second 'checks' the network status.
        """
        while True:
            print('Checking connection...')
            await asyncio.sleep(1)
            await self.do_check()

    async def do_check(self):
        self.connection_state = self.actual_connection_state
        print('Connection state: {}'.format(self.connection_state))

class AppView(ModelView):
    
    model = Instance(App)
    
    def close(self, info, is_ok):
        print('Close')
        try:
            self.model.check_connection_periodically_task.cancel()
        finally:
            return True
    
    def default_traits_view(self):
        view = View(
                    HGroup(
                        VGroup(
                            Item('model.actual_connection_state', show_label=False),
                            label='Actual connection state:',
                            ),
                        VGroup(
                            Item('model.connection_state', style='readonly', show_label=False),
                            label="App thinks the current state is:"
                        ),
                     ),
                    resizable=True
                    )
        return view



def main():
    model = App()
    view = AppView(model=model)
    view.configure_traits()
    
if __name__ == '__main__':
    main()
